import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig } from '@angular/material';
import { CityService } from 'src/app/_shared/_services/index';
import { City } from 'src/app/_shared/_models/index';
import { CityAddComponent } from './add/city-add.component';
import { DialogService } from 'src/app/_shared/_services/dialog.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';




@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html'
})

export class CitiesComponent implements OnInit, AfterViewInit, OnDestroy {

  public dataSource = new MatTableDataSource<City>();
  public displayedColumns: string[] = ['number', 'city', 'lat', 'lon', 'actions'];

  private _getCitiesSub: Subscription;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;

  constructor(private _cityService: CityService, private _dialog: MatDialog, private _dialogService: DialogService, private _router: Router) { }

  ngOnInit() {
    this.getAllCities();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public getAllCities = () => {
    this._getCitiesSub = this._getCitiesSub = this._cityService.getCities()
      .subscribe(res => {
        this.dataSource.data = res['Items'];
      })
  }

  public onSearchClear(): void {
    this.searchKey = '';
    this.doFilter();
  }

  public deleteCity(id: number) {

    this._dialogService.openConfirmDialog('Are you sure to delete this?').afterClosed().subscribe(res => {

      if (res) {
        this._cityService.deleteCity(id).subscribe(
          res => {
            this.getAllCities();
          }
        );
      }
    });
  }

  public redirectToCenter = (city: string, id: number) => {
    let url: string = `/centers/${city}/${id}`;
    this._router.navigate([url]);
  }

  public doFilter(): void {
    this.dataSource.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  public onCreate(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    const dialogRef = this._dialog.open(CityAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(res => {
      this.getAllCities()
    })

  }

  ngOnDestroy() {
    this._getCitiesSub.unsubscribe();
  }

}

