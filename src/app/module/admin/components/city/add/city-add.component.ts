import { Component, OnInit, OnDestroy } from '@angular/core';
import { CityService } from 'src/app/_shared/_services/index';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-city-add',
  templateUrl: './city-add.component.html'
})

export class CityAddComponent implements OnInit, OnDestroy {

  public cityForm: FormGroup;

  public lat: number = null;
  public lon: number = null;

  private _addCitySub: Subscription;

  constructor(public dialogRef: MatDialogRef<CityAddComponent>, private _cityService: CityService, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.cityForm = this._formBuilder.group({
      city: ['', Validators.required],
      location: this._formBuilder.group({
        lat: ['', [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]],
        lon: ['', [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]]
      })
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(form: NgForm) {
    this._addCitySub = this._cityService.addCity(form)
      .subscribe(res => {
        this.onNoClick();
      });
  }

  public onChooseLocation(event) {
    this.lat = event.coords.lat;
    this.lon = event.coords.lng;
    this.cityForm.get('location.lat').setValue(event.coords.lat)
    this.cityForm.get('location.lon').setValue(event.coords.lng)
  }

  ngOnDestroy() {
    if (this._addCitySub) {
      this._addCitySub.unsubscribe()
    }
  }

}