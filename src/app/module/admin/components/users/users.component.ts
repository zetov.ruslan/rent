import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import { UsersService } from 'src/app/_shared/_services/index';
import { NgxSpinnerService } from 'ngx-spinner';

import { Users } from 'src/app/_shared/_models/index';

import { Router } from "@angular/router";
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})

export class UsersComponent implements OnInit, AfterViewInit, OnDestroy {

  public dataSource = new MatTableDataSource<Users>();
  public displayedColumns: string[] = ['number', 'id', 'fullName', 'created_at', 'actions'];

  private _getUserSub: Subscription;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;


  constructor(private _spinner: NgxSpinnerService, private _usersService: UsersService, private _router: Router) { }

  ngOnInit() {
    this._spinner.show();
    this.getAllUsers();

  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public getAllUsers(): void {
    this._getUserSub = this._usersService.getUsers()
      .subscribe(res => {
        this.dataSource.data = res as Users[];
        setTimeout(() => {
          this._spinner.hide();
      }, 500);
      })
  }

  public onSearchClear(): void {
    this.searchKey = '';
    this.doFilter();
  }

  public doFilter(): void {
    this.dataSource.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  public redirectToUser = (id: number) => {
    let url: string = `/booking/user/${id}`;
    this._router.navigate([url]);
  }

  ngOnDestroy() {
    this._getUserSub.unsubscribe();
  }

}

