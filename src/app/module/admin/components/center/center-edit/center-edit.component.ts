import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, Inject, OnInit, OnDestroy } from '@angular/core';

import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CenterService } from 'src/app/_shared/_services/center.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-center-edit',
  templateUrl: './center-edit.component.html'
})
export class CenterEditComponent implements OnInit, OnDestroy {

  public centerForm: FormGroup;

  private _updateCenterSub: Subscription;

  constructor(public dialogRef: MatDialogRef<CenterEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private _formBuilder: FormBuilder, private _centerService: CenterService) { }


  ngOnInit() {
    this.centerForm = this._formBuilder.group({
      id: [''],
      cost: [this.data.cost, [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]],
      refundDeposit: [this.data.refundDeposit, [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]],
      bikesCount: [this.data.bikesCount, [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]]
    });
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(form: object, id: number) {
    this._updateCenterSub = this._centerService.updateCenter(form, id)
      .subscribe(res => {
        this.onNoClick();
      });
  }

  ngOnDestroy() {
    if (this._updateCenterSub) {
      this._updateCenterSub.unsubscribe();
    }
  }

}
