import { Component, OnInit, OnDestroy } from '@angular/core';
import { CenterService } from 'src/app/_shared/_services/center.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Center } from 'src/app/_shared/_models/Center';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-center-add',
  templateUrl: './center-add.component.html'
})
export class CenterAddComponent implements OnInit, OnDestroy {

  public centerForm: FormGroup;
  private _addCenterSub: Subscription;

  constructor(public dialogRef: MatDialogRef<CenterAddComponent>, private _centerService: CenterService, private _formBuilder: FormBuilder) { }

  ngOnInit() {

    this.centerForm = this._formBuilder.group({
      city: ['', Validators.required],
      name: ['', Validators.required],
      cost: ['', [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]],
      refundDeposit: ['', [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]],
      location: this._formBuilder.group({
        lat: ['', [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]],
        lon: ['', [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]]
      }),
      bikesCount: ['', [Validators.required, Validators.pattern(/^(\d*\.)?\d+$/)]],
      availableTime: this._formBuilder.group({
        monday: this._formBuilder.group({
          openTime: [],
          closeTime: [],   
          isOpen: [],
        }),
        tuesday: this._formBuilder.group({
          openTime: [],
          closeTime: [],
          isOpen: []
        }),
        wednesday: this._formBuilder.group({
          openTime: [],
          closeTime: [],
          isOpen: []
        }),
        thursday: this._formBuilder.group({
          openTime: [],
          closeTime: [],
          isOpen: []
        }),
        friday: this._formBuilder.group({
          openTime: [],
          closeTime: [],
          isOpen: []
        }),
        saturday: this._formBuilder.group({
          openTime: [],
          closeTime: [],
          isOpen: []
        }),
        sunday: this._formBuilder.group({
          openTime: [],
          closeTime: [],
          isOpen: []
        })
      }),    
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  

  onSubmit(form: Center) {
    this._addCenterSub = this._centerService.addCenter(form)
      .subscribe(res => {
        this.onNoClick();
      });
  }

  ngOnDestroy() {
    if(this._addCenterSub) {
      this._addCenterSub.unsubscribe()
    }
  }

}
