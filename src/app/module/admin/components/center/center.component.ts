import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { ActivatedRoute } from "@angular/router";
import { CenterService, DialogService } from 'src/app/_shared/_services/index';
import { Center } from 'src/app/_shared/_models/Center';
import { CenterAddComponent } from './center-add/center-add.component';
import { CenterEditComponent } from './center-edit/center-edit.component';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-center',
  templateUrl: './center.component.html'
})

export class CenterComponent implements OnInit , OnDestroy {
  public dataSource = new MatTableDataSource<Center>();
  public displayedColumns = ['id', 'name', 'bikesCount', 'cost', 'refundDeposit', 'location', 'actions'];
  public city: string;
  public id: number;
  public center: Observable<Center>;

  private _getAllCentersSub: Subscription;


  constructor(private _centerService: CenterService, private _dialog: MatDialog, private _route: ActivatedRoute, private _dialogService: DialogService) { }

  ngOnInit() {
    this._route.paramMap.subscribe(param => {
      this.city = param.get('city');
      this.id = + param.get('id')
    })
    
    this.getAllCenters(this.city);

  }

  public getAllCenters(city: string) {
    this._getAllCentersSub = this._centerService.getAllCenters(city)
    .subscribe(res => {
      this.dataSource.data = res['Items'];
      this.center = res['Items']
    })
  }

  public onCreate() : void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    const dialogRef = this._dialog.open(CenterAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(res => {
      this.getAllCenters(this.city)
    })
  }

  public onEdit(id: number, bikesCount: number, cost: number, refundDeposit: number){
  
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    const dialogRef = this._dialog.open(CenterEditComponent,{
      data: {id: id, bikesCount: bikesCount, cost: cost, refundDeposit: refundDeposit}
    });

    dialogRef.afterClosed().subscribe(res => {
      this.getAllCenters(this.city)
    })
  }

  public deleteCenter(id: number) {
 
    this._dialogService.openConfirmDialog('Are you sure to delete this?').afterClosed().subscribe(res => {

      if(res) {
        this._centerService.deleteCenter(id).subscribe(
          res => {
            this.getAllCenters(this.city)
          }
        );  
      }
    });
  }


  ngOnDestroy() {
    this._getAllCentersSub.unsubscribe();
  }

}
