import { Component, OnInit, OnDestroy } from '@angular/core';
import { CenterService, BookingService } from 'src/app/_shared/_services/index';
import { Center } from 'src/app/_shared/_models/Center';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html'
})

export class StatisticComponent implements OnInit, OnDestroy {

  public city: string;
  public centers: Center;
  public bookings: Array<any>;
  public centerError: string;
  public centerBookings: Array<string>;
  public centerBookingsActive: Array<string>;

  private _getAllCentersSub: Subscription;
  private _getAllBookingsByCitySub: Subscription;

  constructor(private _centerService: CenterService, private _bookingService: BookingService) { }

  ngOnInit() {
  }

  public onEnter(value: string) {
    this.city = value.toLowerCase();
    this.getAllCenters(this.city);
    this.getAllBookingsByCity(this.city);
  }

  public getAllCenters(city: string) {
    this._getAllCentersSub = this._centerService.getAllCenters(city)
      .subscribe(res => {
        this.centers = res['Items'];
        this.centerError = '';
      }, err => { this.centerError = err })
  }

  public getAllBookingsByCity(city: string) {
    this._getAllBookingsByCitySub = this._bookingService.getBookingsByCity(city)
      .subscribe(res => {
        this.bookings = res;
      })
  }

  public getAllCenterBookings(id: string) {
    this.centerBookings = [];
    this.centerBookingsActive = [];
    for (let item of this.bookings) {
      if (id == item.centerId && item.status == 'active') {
        this.centerBookingsActive.push(item)
      } else if (id == item.centerId) {
        this.centerBookings.push(item)
      }
    }
  }

  ngOnDestroy() {
    if (this._getAllCentersSub && this._getAllBookingsByCitySub) {
      this._getAllCentersSub.unsubscribe();
      this._getAllBookingsByCitySub.unsubscribe();
    }

  }
}
