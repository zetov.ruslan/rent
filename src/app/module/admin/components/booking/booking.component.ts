import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { BookingService } from 'src/app/_shared/_services/index';
import { Booking } from 'src/app/_shared/_models/index';
import { ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html'
})
export class BookingComponent implements OnInit, AfterViewInit, OnDestroy {

  public dataSource = new MatTableDataSource<Booking>();
  public displayedColumns: string[] = ['centerId', 'amount', 'city', 'status', 'created_at', 'endTime'];
  public id: string;
  public status: string[] = ['all', 'active', 'completed'];

  private _getBookingsSub: Subscription;


  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _bookingService: BookingService, private _route: ActivatedRoute) { }

  ngOnInit() {
    this._route.paramMap.subscribe(param => {
      this.id = param.get('id')
    })
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  public getUserBookings(id: string, status: string): void {
    this._getBookingsSub = this._bookingService.getBookings(id, status)
      .subscribe(res => {
        this.dataSource.data = res;
      })
  }

  ngOnDestroy(): void {
    if(this._getBookingsSub) {
      this._getBookingsSub.unsubscribe();
    }
  }
}
