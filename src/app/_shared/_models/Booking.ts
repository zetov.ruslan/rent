export interface Booking {
	centerId: string;
  city: string;
	status: string;
}