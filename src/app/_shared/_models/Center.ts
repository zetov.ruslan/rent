export interface Center {
	id: string;
  city: string;
	cost: number;
  refundDeposit: number;
	name: string;
	bikesCount: number;
	location: Location;
	availableTime: AvailableTime
}

export class Location {
	lat: string;
	lon: string;
	address: string;
}

export class AvailableTime {
	closeTime: string;
	openTime: string;
	isOpen: boolean;
}