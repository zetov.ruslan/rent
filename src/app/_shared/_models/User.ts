export interface User {
	item: Item;
	access_token: string;
	expires_in: string;
}

export class Item {
	created_at: number;
	refresh_token: string;
	disabledBooking: boolean;
	project: string;
	fullName: string;
	id: string;
	email: string;
}