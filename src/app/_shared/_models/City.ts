export interface Cities {
	Item: City
}

export interface City {
	id: number;
	city: string;
	project: string;
	location: Location
}

export class Location {
	lon: number;
	lat: number;
}