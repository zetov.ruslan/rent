import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../_services/index';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private _authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 403) {
                this._authenticationService.getNewAcessToken()
                .subscribe(user => {
                    
                    localStorage.setItem('refresh_token', JSON.stringify(user.user.refresh_token))
                    localStorage.setItem('currentUser', JSON.stringify(user))
                    
                  });
                  location.reload();
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}