import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Center } from '../_models/Center';
import { HttpClient } from '@angular/common/http';


@Injectable({providedIn: 'root'})
export class CenterService {
  private _apiUrl = 'https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0/centers';

  dataChange: BehaviorSubject<Center[]> = new BehaviorSubject<Center[]>([]);
  
  dialogData: any;

  constructor (private _http: HttpClient) {}

	public getAllCenters(city: string): Observable<Center[]> {
    return this._http.get<Center[]>(`${this._apiUrl}/${city}`)
  }

  public addCenter(center: object): Observable<Center> {
    return this._http.post<Center>(this._apiUrl, center)
  }

  public updateCenter(center: object, id: number): Observable<Center> {
    return this._http.post<Center>(`${this._apiUrl}/object/${id}/update`, center);
  }

  public deleteCenter(id: number) {
    return this._http.delete(`${this._apiUrl}/${id}`);
  }

}
