import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../_models/index';

@Injectable({ providedIn: 'root' })

export class AuthenticationService {
	private _currentUserSubject: BehaviorSubject<User>;
	public currentUser: Observable<User>;

	private _apiUrl: string = 'https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0';

	constructor(private _http: HttpClient) {
		this._currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
		this.currentUser = this._currentUserSubject.asObservable();
	}

	public get currentUserValue(): User {
		return this._currentUserSubject.value;
	}

	public login(email: string, password: string) {
		return this._http.post<any>(`${this._apiUrl}/auth/signin`, { email, password })
			.pipe(map(user => {

				if (user && user.access_token) {
					localStorage.setItem('refresh_token', JSON.stringify(user.Item.refresh_token))
					localStorage.setItem('currentUser', JSON.stringify(user));
					this._currentUserSubject.next(user);
				}

				return user;
			}));
	}

	public getRefreshToken(): string {
		return localStorage.getItem('refresh_token').replace(/"/g, "");
	}

	public getNewAcessToken() {
		return this._http.post<any>(`${this._apiUrl}/auth/refresh
		`, {refresh_token: this.getRefreshToken()});
	}

	public logout() {
		localStorage.removeItem('currentUser')
		localStorage.removeItem('refresh_token');
		this._currentUserSubject.next(null);
	}
}