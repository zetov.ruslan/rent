import { Injectable } from '@angular/core';
import { Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { City, Cities } from '../_models/index';
import { NgForm } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class CityService {

  private _httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  private _apiUrl = "https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0/cities";

  constructor(private _http: HttpClient) { }

  public getCities(): Observable<Cities[]> {
    return this._http.get<Cities[]>(this._apiUrl)
  }

  public getCity(id: number): Observable<City> {
    const url = `${this._apiUrl}/${id}`;
    return this._http.get<City>(url)
  }

  public addCity(city: NgForm): Observable<City> {
    return this._http.post<City>(this._apiUrl, city, this._httpOptions)
  }

  public deleteCity(id: number){
    return this._http.delete(`${this._apiUrl}/${id}`)
  }

}