import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Booking } from '../_models/Booking';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private _apiUrl = 'https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0/bookings';

  constructor(private _http: HttpClient) { }

  public getBookings(id: string, status: string): Observable<Booking[]> {
    return this._http.get<Booking[]>(`${this._apiUrl}/user/${id}/status/${status}`)
  }

  public getBookingsByCity(city: string): Observable<Booking[]> {
    return this._http.get<Booking[]>(`${this._apiUrl}/city/${city}`)
  }
}
