import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Users } from '../_models/index';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private _apiUrl: string = 'https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0/users/all';


  constructor(private _http: HttpClient) { }

  getUsers(): Observable<Users[]> {
    return this._http.get<Users[]>(this._apiUrl)
  }

}
