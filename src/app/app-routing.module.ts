import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AuthGuard } from './_shared/_guards/index';


import { LoginComponent } from 'src/app/module/auth/login/login.component';
import { DashboardComponent } from './_shared/_components/dashboard/dashboard.component';
import { AppComponent } from './app.component';
import { UsersComponent } from 'src/app/module/admin/components/users/users.component';
import { CitiesComponent } from 'src/app/module/admin/components/city/city-list.component';
import { CenterComponent } from 'src/app/module/admin/components/center/center.component';
import { BookingComponent } from 'src/app/module/admin/components/booking/booking.component';
import { NotfoundComponent } from './_shared/_components/notfound/notfound.component';
import { StatisticComponent } from 'src/app/module/admin/components/statistic/statistic.component';


const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {path: '', redirectTo: 'login', pathMatch:'full'},
      {path: 'login', component: LoginComponent}
    ]
  },
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {path:'cities', component: CitiesComponent},
      {path:'centers/:city/:id', component: CenterComponent},
      {path:'users', component: UsersComponent},
      {path:'booking/user/:id', component: BookingComponent},
      {path:'statistic', component: StatisticComponent}
    ]
  },

  { path: '**',  component: NotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
