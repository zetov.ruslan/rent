import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { CustomMaterialModule } from './_shared/material.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';


import { JwtInterceptor, ErrorInterceptor } from './_shared/_helpers/index';


import { AppComponent } from './app.component';
import { LoginComponent } from 'src/app/module/auth/login/login.component';
import { DashboardComponent } from './_shared/_components/dashboard/dashboard.component';
import { CityAddComponent } from 'src/app/module/admin/components/city/add/city-add.component';
import { UsersComponent } from 'src/app/module/admin/components/users/users.component';
import { CitiesComponent } from 'src/app/module/admin/components/city/city-list.component';
import { ConfirmDialogComponent } from './_shared/_components/confirm-dialog/confirm-dialog.component';
import { CenterComponent } from 'src/app/module/admin/components/center/center.component';
import { CenterAddComponent } from 'src/app/module/admin/components/center/center-add/center-add.component';
import { CenterEditComponent } from 'src/app/module/admin/components/center/center-edit/center-edit.component';
import { BookingComponent } from 'src/app/module/admin/components/booking/booking.component';
import { NotfoundComponent } from './_shared/_components/notfound/notfound.component';
import { StatisticComponent } from 'src/app/module/admin/components/statistic/statistic.component';



@NgModule({
   declarations: [
      AppComponent,
      LoginComponent,
      DashboardComponent,
      CityAddComponent,
      UsersComponent,
      CitiesComponent,
      ConfirmDialogComponent,
      CenterComponent,
      CenterAddComponent,
      CenterEditComponent,
      BookingComponent,
      NotfoundComponent,
      StatisticComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      CustomMaterialModule,
      AppRoutingModule,
      FormsModule,
      NgxSpinnerModule,
      AgmCoreModule.forRoot({
         apiKey: 'AIzaSyBRvHsahmBuGNnVym6rvSHN_P6rjAfQOvQ'
       }),
      LayoutModule,
      MatToolbarModule,
      MatButtonModule,
      MatSidenavModule,
      MatIconModule,
      MatListModule
   ],
   providers: [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }

   ],
   bootstrap: [
      AppComponent
   ],
   entryComponents: [
      CityAddComponent,
      ConfirmDialogComponent,
      CenterAddComponent,
      CenterEditComponent
   ]
})
export class AppModule { }
