import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_shared/_services/index';
import { User } from './_shared/_models/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  public currentUser: User;

  constructor(
    private _router: Router,
    private _authenticationService: AuthenticationService
  ) {
    this._authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
    this._authenticationService.logout();
    this._router.navigate(['/login']);
  }
}
