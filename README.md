# Rent

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

# Getting started

## Making requests to the backend API

1. Make sure you have the Angular CLI installed globally.
2. Run `npm install` to resolve all dependencies (might take a minute).
3. Run `ng serve` for a dev server. 
4. Navigate to `http://localhost:4200/`. 
The app will automatically reload if you change any of the source files.

# Functionality overview
The example application is a rent admin-dashboard.

## General functionality:
1. Authenticate admin via JWT
2. *R*D users
3. CRUD rent centers in the city
4. *RUD bookings.